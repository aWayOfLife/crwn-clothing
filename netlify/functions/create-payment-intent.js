require('dotenv').config();
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);

const USD_TO_INR = 81.67;

exports.handler = async (event) => {
    try {
        const { amount } = JSON.parse(event.body);

        const paymentIntent = await stripe.paymentIntents.create({
            amount: amount * USD_TO_INR,
            currency: 'inr',
            payment_method_types: ['card'],
            description: 'Crwn clothing transaction',
            customer: process.env.STRIPE_CUSTOMER_ID
        });

        return {
            statusCode: 200,
            body: JSON.stringify({ paymentIntent }),
        };
    } catch (error) {
        console.log({ error });

        return {
            status: 400,
            body: JSON.stringify({ error }),
        };
    }
};