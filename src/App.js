import {Route, Routes} from 'react-router-dom';
import './App.css';
import HomePage from './routes/home-page/HomePage';
import ShopPage from './routes/shop-page/ShopPage';
import SignInSignUpPage from './routes/sign-in-sign-up-page/SignInSignUpPage';
import React from 'react';
import Navigation from './components/navigation/Navigation';
import CheckoutPage from './routes/checkout-page/CheckoutPage';

const App = () => {
  return (
    <div>
      <Routes>
        <Route path="/" element={<Navigation />}>
          <Route index element={<HomePage />}></Route>
          <Route path="/shop/*" element={<ShopPage />} />
          <Route path="/signin" element={<SignInSignUpPage />} />
          <Route path='/checkout' element={<CheckoutPage/>}/>
        </Route>
      </Routes>
    </div>
  );
};

export default App;
