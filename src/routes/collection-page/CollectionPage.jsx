import './CollectionPage.scss'

import React, { useContext, useEffect, useState } from 'react'
import { useParams } from 'react-router'
import { CollectionsContext } from '../../context/CollectionsContext'
import CollectionItem from '../../components/collection-item/CollectionItem'

const CollectionPage = () => {
    const { collection } = useParams()
    const { collectionsMap } = useContext(CollectionsContext)
    const [products, setProducts] = useState([])
    useEffect(() => {
        setProducts(collectionsMap[collection])
    }, [collection, collectionsMap])

    return (
        <>
            <div className='title'>{collection.toUpperCase()}</div>
            <div className='collection-container'>
                {
                    products && products.map((item) => <CollectionItem key={item.id} item={item}></CollectionItem>)
                }
            </div>
        </>

    )
}

export default CollectionPage