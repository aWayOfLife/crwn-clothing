import SignIn from '../../components/sign-in/SignIn';
import SignUp from '../../components/sign-up/SignUp';
import './SignInSignUpPage.scss';

const SignInSignUpPage = () => {
    return (
        <div className='sign-in-and-sign-up'>
            <SignIn></SignIn>
            <SignUp></SignUp>
        </div>
    )
}

export default SignInSignUpPage
