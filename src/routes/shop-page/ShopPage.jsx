import React from 'react'
import { Route, Routes } from 'react-router-dom';
import CollectionPage from '../collection-page/CollectionPage';
import CollectionsPage from '../collections-page/CollectionsPage';

const ShopPage = () => {
    return (
        <Routes>
            <Route index element={<CollectionsPage/>}></Route>
            <Route path="/:collection" element={<CollectionPage />} />
        </Routes>
    )
}

export default ShopPage