import Directory from '../../components/directory/Directory';
import './HomePage.scss';

const HomePage = () => {
    return (
        <div className="home-page">
            <Directory></Directory>
        </div>
    )
}

export default HomePage

