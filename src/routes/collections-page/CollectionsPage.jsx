import './CollectionsPage.scss'

import React, { Fragment, useContext } from 'react'
import CollectionPreview from '../../components/collection-preview/CollectionPreview';
import { CollectionsContext } from '../../context/CollectionsContext';

const CollectionsPage = () => {
    const { collectionsMap } = useContext(CollectionsContext)
    return (
        <>
            {
                Object.keys(collectionsMap).map(title => (
                    <Fragment key={title}>
                        <div className='shop-page'>
                            <CollectionPreview key={title} items={collectionsMap[title].slice(0, 4)} title={title}></CollectionPreview>
                        </div>
                    </Fragment>
                ))
            }

        </>
    )
}

export default CollectionsPage