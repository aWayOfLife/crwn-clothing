import React, { useEffect, useContext } from 'react'
import CheckoutItem from '../../components/checkout-item/CheckoutItem';
import PaymentForm from '../../components/payment-form/PaymentForm';
import { CartContext } from '../../context/CartContext';
import './CheckoutPage.scss';

const CheckoutPage = () => {
    const { isCartOpen, setIsCartOpen, cartItems, cartTotal } = useContext(CartContext)
    useEffect(() => {
        isCartOpen && setIsCartOpen(false)
    }, [])

    return (
        <div className='checkout-container'>
            <div className='checkout-header'>
                <div className='header-block'><span>Product</span></div>
                <div className='header-block'><span>Description</span></div>
                <div className='header-block'><span>Quantity</span></div>
                <div className='header-block'><span>Price</span></div>
                <div className='header-block'><span>Remove</span></div>
            </div>
            {
                cartItems.map(cartItem =>
                    <CheckoutItem key={cartItem.id} cartItem={cartItem}>
                    </CheckoutItem>
                )
            }
            <span className='total'>Total: {`$${cartTotal}`}</span>
            <PaymentForm></PaymentForm>
        </div>
    )
}

export default CheckoutPage