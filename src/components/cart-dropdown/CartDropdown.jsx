import { CartContext } from '../../context/CartContext';
import CartItem from '../cart-item/CartItem';
import CustomButton from '../custom-button/CustomButton';
import { useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import { CartDropdownContainer, CartItems, EmptyMessage } from './CartDropdown.styles';

const CartDropdown = () => {
    const { cartItems, addItemToCart } = useContext(CartContext)
    const navigate = useNavigate()
    const handleGoToCheckout = (event) => {
        navigate('/checkout')
    }
    return (
        <CartDropdownContainer>
            <CartItems>
                {
                    cartItems.length ? cartItems.map(item => <CartItem key={item.id} cartItem={item}></CartItem>) :
                    (
                        <EmptyMessage>Your cart is empty</EmptyMessage>
                    )
                }
            </CartItems>
            <CustomButton disabled={!cartItems?.length} onClick={handleGoToCheckout}>GO TO CHECKOUT</CustomButton>
        </CartDropdownContainer>
    )
}

export default CartDropdown
