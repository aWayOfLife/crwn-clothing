import { FormInputLabel, Group, Input } from './FormInput.styles';

const FormInput = ({ handleChange, label, ...otherProps}) => {
    return (
        <Group>
            <Input onChange={handleChange} {...otherProps} />
            {
                label ? 
                    <FormInputLabel shrink={otherProps.value.shrink}>{label}</FormInputLabel>
                :null
            }
        </Group>
    )
}

export default FormInput
