import './SignIn.scss';

import React, { Component } from 'react'
import FormInput from '../form-input/FormInput';
import CustomButton from '../custom-button/CustomButton';

import { signInUserWithEmailAndPassword, signInWithGoogle } from '../../utils/firebase/firebase.utils';

export default class SignIn extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: ''
        }
    }

    handleSubmit = async event => {
        event.preventDefault();
        const { email, password } = this.state;

        try {
            await signInUserWithEmailAndPassword(email, password);
            this.setState({
                email: '',
                password: ''
            });
        } catch (error) {
            console.log(error);
        }
    }

    handleChange = event => {
        const { value, name } = event.target;
        this.setState({ [name]: value });
    }

    handleSignInWithGoogle = async event => {
        await signInWithGoogle();
    }

    render() {
        return (
            <div className="sign-in">
                <h2>I already have an account</h2>
                <span>
                    Sign in with your email and password
                </span>
                <form onSubmit={this.handleSubmit}>
                    <FormInput label="Email" name="email" type="email" value={this.state.email} handleChange={this.handleChange} required></FormInput>
                    <FormInput label="Password" name="password" type="password" value={this.state.password} handleChange={this.handleChange} required></FormInput>
                    <div className="buttons">
                        <CustomButton type="submit">Sign In</CustomButton>
                        <CustomButton onClick={this.handleSignInWithGoogle} buttonType='google'>Sign In With Google</CustomButton>
                    </div>
                </form>

            </div>
        )
    }
}
