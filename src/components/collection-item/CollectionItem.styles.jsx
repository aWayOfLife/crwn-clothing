import styled from "styled-components";
import { BaseButton } from "../custom-button/CustomButton.styles";

export const BackgroundImage = styled.div`
  width: 100%;
  height: 100%;
  background-size: cover;
  background-position: center;
  background-image: ${({imageUrl}) => `url(${imageUrl})`};
  background-repeat: no-repeat;
`

export const CollectionItemContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 350px;
  align-items: center;
  position: relative;
  ${BaseButton} {
    width: 80%;
    opacity: 0.7;
    position: absolute;
    top: 255px;
    display: none;
  }
  &:hover {
    ${BackgroundImage} {
      opacity: 0.8;
    }
    ${BaseButton} {
      display: flex;
      opacity: 0.85;
    }
  }
`

export const CollectionItemFooter = styled.div`
  width: 100%;
  height: 5%;
  display: flex;
  justify-content: space-between;
  font-size: 18px;
`
export const Name = styled.div`
  width: 90%;
  margin-bottom: 15px;
`
export const Price = styled.div`
  text-align: end;
  width: 10%;
`
