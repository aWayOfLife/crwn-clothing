import { CartContext } from '../../context/CartContext';
import CustomButton from '../custom-button/CustomButton';
import { useContext } from 'react';
import { BackgroundImage, CollectionItemContainer, CollectionItemFooter, Name, Price } from './CollectionItem.styles';

const CollectionItem = ({ item }) => {
    const { name, price, imageUrl } = item
    const { addItemToCart } = useContext(CartContext)
    const addCollectionItemToCart = () => {
        addItemToCart(item)
    }
    return (
        <CollectionItemContainer>
            <BackgroundImage imageUrl={imageUrl}></BackgroundImage>
            <CollectionItemFooter>
                <Name>{name}</Name>
                <Price className='price'>{price}</Price>
            </CollectionItemFooter>
            <CustomButton onClick={addCollectionItemToCart} buttonType='inverted'>ADD TO CART</CustomButton>
        </CollectionItemContainer>
    )
}

export default CollectionItem
