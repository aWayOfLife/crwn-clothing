import { useNavigate } from 'react-router-dom';
import './MenuItem.scss';

const MenuItem = ({ title, imageUrl, size, linkUrl }) => {
    const navigate = useNavigate();
    return (
        <div className={`${size} menu-item`} onClick={() => navigate(`shop/${linkUrl}`)}>
            <div className="background-image"
                style={{
                    backgroundImage: `url(${imageUrl})`
                }} />
            <div className="content">
                <span className="title">{title.toUpperCase()}</span>
                <span className="subtitle">SHOP NOW</span>
            </div>
        </div>
    )
}

export default MenuItem;
