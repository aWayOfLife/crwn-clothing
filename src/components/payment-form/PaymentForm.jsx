import { useStripe, useElements, CardElement } from "@stripe/react-stripe-js";
import { useContext, useState } from "react";
import { CartContext } from "../../context/CartContext";
import { UserContext } from "../../context/UserContext";
import CustomButton from "../custom-button/CustomButton";
import { FormContainer, PaymentButton, PaymentFormContainer } from "./PaymentForm.styles";

const PaymentForm = () => {
    const [isProcessingPayment, setIsProcessingPayment] = useState(false)
    const { currentUser } = useContext(UserContext)
    const { cartTotal, removeAllItemsFromCart } = useContext(CartContext)
    const stripe = useStripe();
    const elements = useElements();
    const paymentHandler = async (e) => {
        e.preventDefault();
        if (!stripe || !elements) return

        setIsProcessingPayment(true)
        const response = await fetch('/.netlify/functions/create-payment-intent', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ amount: cartTotal * 100 }),
        }).then((res) => res.json());

        const {
            paymentIntent: { client_secret },
        } = response;

        const paymentResult = await stripe.confirmCardPayment(client_secret, {
            payment_method: {
                card: elements.getElement(CardElement),
                billing_details: {
                    name: currentUser?.displayName ?? currentUser?.name ?? 'Crwn Clothing User',
                }
            }
        })
        setIsProcessingPayment(false)

        if (paymentResult.error) {
            alert(JSON.stringify(paymentResult.error))

        } else {
            if (paymentResult.paymentIntent.status === 'succeeded') {
                alert('Payment Successful')
                removeAllItemsFromCart()
            }
        }
    }
    return (
        <PaymentFormContainer>
            <FormContainer onSubmit={paymentHandler}>
                <h2>Credit Card Payment:</h2>
                <CardElement></CardElement>
                <PaymentButton type="submit" buttonType='inverted' isLoading={isProcessingPayment}>Pay Now</PaymentButton>
            </FormContainer>

        </PaymentFormContainer>
    )
}

export default PaymentForm