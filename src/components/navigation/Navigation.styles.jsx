import styled from "styled-components";
import { Link } from "react-router-dom";

export const NavigationContainer = styled.div`
  height: 70px;
  width: 100%;
  display: flex;
  justify-content: space-between;
  margin-bottom: 25px;
`
export const LogoContainer = styled(Link)`
  display: flex;
  justify-content: space-between;
  align-items: center;
`
export const NavigationLinksContainer = styled.div`
  width: 50%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: flex-end;
`
export const NavigationLink = styled(Link)`
  padding: 10px 15px;
  text-align: end;
  font-weight: bold;
  color: black;
  cursor: pointer;
`
