import { Link, Outlet, useNavigate } from 'react-router-dom';
import { NavigationContainer, NavigationLinksContainer, NavigationLink, LogoContainer } from './Navigation.styles.jsx';

import { ReactComponent as Logo } from '../../assets/crown.svg';
import { signOutUser } from '../../utils/firebase/firebase.utils';
import { Fragment, useContext } from 'react';
import { UserContext } from '../../context/UserContext';
import CartIcon from '../cart-icon/CartIcon';
import CartDropdown from '../cart-dropdown/CartDropdown';
import { CartContext } from '../../context/CartContext';

const Navigation = () => {
    const { currentUser } = useContext(UserContext);
    const { isCartOpen } = useContext(CartContext);
    const navigate = useNavigate();

    const handleSignOutUser = async () => {
        await signOutUser();
        navigate('../signin', { replace: true });
    }

    return (
        <Fragment>
            <NavigationContainer>
                <LogoContainer to='/'>
                    <Logo></Logo>
                </LogoContainer>
                <NavigationLinksContainer>
                    <NavigationLink to='/shop'>SHOP</NavigationLink>
                    {
                        currentUser ? <NavigationLink as='span' onClick={() => handleSignOutUser()}>SIGN OUT</NavigationLink> : <NavigationLink to='/signin'>SIGN IN</NavigationLink>
                    }
                    {currentUser && <CartIcon></CartIcon>}
                </NavigationLinksContainer>
                {
                    currentUser && isCartOpen ? <CartDropdown></CartDropdown> : null
                }

            </NavigationContainer>
            <Outlet></Outlet>
        </Fragment>
    )
}

export default Navigation;
