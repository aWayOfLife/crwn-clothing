import { BaseButton, ButtonSpinner, GoogleButton, InvertedButton } from './CustomButton.styles';

const BUTTON_TYPE_CLASSES = {
    base: 'base',
    google: 'google',
    inverted: 'inverted'
}

const getButton = (buttonType = BUTTON_TYPE_CLASSES.base) => (
    {
        [BUTTON_TYPE_CLASSES.base]: BaseButton,
        [BUTTON_TYPE_CLASSES.google]: GoogleButton,
        [BUTTON_TYPE_CLASSES.inverted]: InvertedButton
    }[buttonType]
)

const CustomButton = ({ children, buttonType, isLoading, ...otherProps }) => {
    const Button = getButton(buttonType)
    return (
        <Button disabled={isLoading} {...otherProps}>
            {isLoading ? <ButtonSpinner></ButtonSpinner> : children}
        </Button>
    )
}

export default CustomButton
