import { useNavigate } from 'react-router-dom';
import CollectionItem from '../collection-item/CollectionItem';
import './CollectionPreview.scss';

const CollectionPreview = ({ title, items }) => {
    const navigate = useNavigate();
    return (
        <div className='collection-preview'>
            <h1 className='title' onClick={() => navigate(`${title.toLowerCase()}`)}>{title.toUpperCase()}</h1>
            <div className='preview'>
                {
                    items.filter((item, i) => i < 4).map((item) => (
                        <CollectionItem key={item.id} item={item}></CollectionItem>
                    ))
                }
            </div>
        </div>
    )
}

export default CollectionPreview
