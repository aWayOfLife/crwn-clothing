import { initializeApp } from 'firebase/app';
import { GoogleAuthProvider, getAuth, signInWithPopup, createUserWithEmailAndPassword, signInWithEmailAndPassword, signOut, onAuthStateChanged } from 'firebase/auth';
import { getFirestore, doc, getDoc, setDoc, collection, writeBatch, query, getDocs } from 'firebase/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyBNqr2Ft9XE9Q44BXaaY_6b3BQF9CTQTfc',
  authDomain: 'crwn-clothing-48387.firebaseapp.com',
  projectId: 'crwn-clothing-48387',
  storageBucket: 'crwn-clothing-48387.appspot.com',
  messagingSenderId: '3881364090',
  appId: '1:3881364090:web:7e51913df9af1670d1a9c3',
};

initializeApp(firebaseConfig);
const provider = new GoogleAuthProvider();

provider.setCustomParameters({ prompt: 'select_account' });

export const auth = getAuth();
export const db = getFirestore();

export const signInWithGoogle = async () => await signInWithPopup(auth, provider);

export const signUpUserWithEmailAndPassword = async (email, password) => await createUserWithEmailAndPassword(auth, email, password);

export const signInUserWithEmailAndPassword = async (email, password) => await signInWithEmailAndPassword(auth, email, password);

export const signOutUser = async () => await signOut(auth);

export const createUserProfileDocument = async (userAuth, additionalData) => {
  if (!userAuth) return;
  const userDocRef = doc(db, 'users', userAuth.uid);
  const userDocSnap = await getDoc(userDocRef);
  if (!userDocSnap.exists()) {
    const { displayName, email } = userAuth;
    const createdAt = new Date();
    try {
      await setDoc(userDocRef, {
        displayName,
        email,
        createdAt,
        ...additionalData,
      });
    } catch (error) {
      console.log(error);
    }
  }

  return userDocRef;
};

export const onAuthStateChangedListener = (callback) => onAuthStateChanged(auth, (data) => callback(data));

export const addCollectionAndDocuments = async (collectionKey, objectsToAdd) => {
  const collectionRef = collection(db, collectionKey)
  const batch = writeBatch(db)

  objectsToAdd.forEach(object => {
    const docRef = doc(collectionRef, object.title.toLowerCase())
    batch.set(docRef, object)
  })

  await batch.commit()
}

export const getCollectionsAndDocuments = async () => {
  const collectionRef = collection(db, 'categories')
  const q = query(collectionRef)
  const querySnapshot = await getDocs(q)

  const collectionMap = querySnapshot.docs.map(docSnapshot => docSnapshot.data()).reduce((acc, docData) => {
    const { title, items, id, routeName } = docData
    acc[title.toLowerCase()] = items
    return acc
  }, {});
  return collectionMap
}