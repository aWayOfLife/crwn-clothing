import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import './index.css';
import App from './App';
import { createRoot } from 'react-dom/client';
import { UserProvider } from './context/UserContext';
import { CartProvider } from './context/CartContext';
import { CollectionsProvider } from './context/CollectionsContext';
import { Elements } from '@stripe/react-stripe-js';
import { stripePromise } from './utils/stripe/stripe.utils';

const container = document.getElementById('root');
const root = createRoot(container);
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <UserProvider>
        <CollectionsProvider>
          <CartProvider>
            <Elements stripe={stripePromise}>
              <App />
            </Elements>
          </CartProvider>
        </CollectionsProvider>
      </UserProvider>
    </BrowserRouter>
  </React.StrictMode>,
);
