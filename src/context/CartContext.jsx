import { createContext, useState, useEffect } from "react";
import { useNavigate, useMatch } from "react-router";

const addCartItem = (cartItems, productToAdd) => {
    const existingItem = cartItems.find(item => item.id === productToAdd.id)
    if (existingItem) {
        return [...cartItems.map(cartItem => cartItem.id === productToAdd.id ? { ...cartItem, quantity: cartItem.quantity + 1 } : cartItem)]
    }
    return [...cartItems, { ...productToAdd, quantity: 1 }]
}

const removeCartItem = (cartItems, productToRemove, removeAll = false) => {
    const existingItem = cartItems.find(item => item.id === productToRemove.id)
    if (existingItem) {
        return [...cartItems.map(cartItem => cartItem.id === productToRemove.id ? { ...cartItem, quantity: cartItem.quantity - (removeAll ? cartItem.quantity : 1) } : cartItem)].filter(item => item.quantity > 0)
    }
    return [...cartItems]
}

const removeAllCartItems = (cartItems) => {
    return [...cartItems.map(cartItem => ({ ...cartItem, quantity: 0 }))].filter(item => item.quantity > 0)
}

export const CartContext = createContext({
    isCartOpen: null,
    setIsCartOpen: () => null,
    cartItems: [],
    addItemToCart: () => null,
    removeItemFromCart: () => null,
    cartCount: 0,
    cartTotal: 0,
    removeAllItemsFromCart: () => null
})

export const CartProvider = ({ children }) => {
    const navigate = useNavigate();
    const isCheckoutPage = useMatch('/checkout');
    const [isCartOpen, setIsCartOpen] = useState(false)
    const [cartItems, setCartItems] = useState([])
    const [cartCount, setCartCount] = useState(0)
    const [cartTotal, setCartTotal] = useState(0)
    const addItemToCart = (productToAdd) => {
        setCartItems(addCartItem(cartItems, productToAdd))
    }
    const removeItemFromCart = (productToRemove, removeAll = false) => {
        setCartItems(removeCartItem(cartItems, productToRemove, removeAll))
    }
    const removeAllItemsFromCart = () => {
        setCartItems(removeAllCartItems(cartItems))
    }
    const value = {
        isCartOpen,
        setIsCartOpen,
        addItemToCart,
        cartItems,
        cartCount,
        removeItemFromCart,
        cartTotal,
        removeAllItemsFromCart
    }
    useEffect(() => {
        setCartCount(cartItems.reduce((total, cartItem) => total + cartItem.quantity, 0))
        setCartTotal(cartItems.reduce((total, cartItem) => total + (cartItem.quantity * cartItem.price), 0))

    }, [cartItems])

    useEffect(() => {
        if (isCheckoutPage && !cartCount) {
            navigate('../', { replace: true })
        }
    }, [cartCount])


    return (
        <CartContext.Provider value={value}>{children}</CartContext.Provider>
    )
}
