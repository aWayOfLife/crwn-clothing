import { createContext, useState, useEffect } from 'react';
import { useMatch, useNavigate } from 'react-router';
import { createUserProfileDocument, onAuthStateChangedListener } from '../utils/firebase/firebase.utils';

// value we want to access
export const UserContext = createContext({
    currentUser: null,
    setCurrentUser: () => null
});


export const UserProvider = ({ children }) => {
    const [currentUser, setCurrentUser] = useState(null);
    const navigate = useNavigate();
    const isSignInPage = useMatch('/signin');

    const value = {
        currentUser,
        setCurrentUser
    }
    useEffect(() => {
        const unsubscribe = onAuthStateChangedListener(async (user) => {
            setCurrentUser(user);
            if (user) {
                await createUserProfileDocument(user);
                if (isSignInPage) {
                    navigate('../', { replace: true });
                }
            }
        });
        return unsubscribe;
    }, [currentUser]);

    return (
        <UserContext.Provider value={value}>{children}</UserContext.Provider>
    );
};