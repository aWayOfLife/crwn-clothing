import { createContext, useState, useEffect } from 'react';
import { getCollectionsAndDocuments } from '../utils/firebase/firebase.utils';

export const CollectionsContext = createContext({
    collectionsMap: null,
    setCollectionsMap: () => null
})


export const CollectionsProvider = ({ children }) => {
    const [collectionsMap, setCollectionsMap] = useState({})
    const value = {
        collectionsMap,
        setCollectionsMap
    }

    useEffect(() => {
        const getCollectionsMap = async () => {
            const collectionMap = await getCollectionsAndDocuments()
            setCollectionsMap(collectionMap)
        }
        getCollectionsMap()
    }, [])


    return (
        <CollectionsContext.Provider value={value}>{children}</CollectionsContext.Provider>
    )
}